import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

// This is a class for Home Page
public class TestJIRA {
   
	private final WebDriver driver;

	// TestJIRA Constructor
    public TestJIRA(WebDriver driver) {
        this.driver = driver;
    }
    
    // This method is a Test method
    public boolean createIssue() {

    	// wait for 2 mins to create link
		WebDriverWait WDwait = new WebDriverWait(driver, 120);
		WDwait.until(ExpectedConditions.elementToBeClickable(By.id("create_link")));
		
        //  Click the create issue link
        WebElement webElement = driver.findElement(By.id("create_link"));        
        webElement.click();

        // Wait for page Load 
		WDwait = new WebDriverWait(driver, 120);
		WDwait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary")));

        // Fill in the summary
		webElement = driver.findElement(By.id("summary"));
		webElement.sendKeys("This issue is created for task 3");
             
        // Click the create issue button
		webElement = driver.findElement(By.id("create-issue-submit"));
		webElement.click();
        	
        // Test for and return result
        if (driver.findElement(By.className("aui-message success closeable"))!=null) {
        	return true;
        } else {    	
        	return false;
        }
    }
	    
}