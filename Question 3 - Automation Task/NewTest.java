import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class NewTest {

	public static void main(String[] args) {

        WebDriver driver = new FirefoxDriver();

        //This is to set starting page reference
        driver.get("https://jira.atlassian.com/login.jsp?os_destination=%2Fsecure%2FDashboard.jspa");
        LoginPage loginPage = new LoginPage(driver);
        
        // Create JIRA Object when login is successful
        TestJIRA testJira = loginPage.loginAs("bhagyashree.bajaj@gmail.com", "XXXXXXXX");

        // Execute test case 
        if (testJira.createIssue()) {
        	System.out.println("Issue created");
        } else {
        	System.out.println("Try Again");
        }        	

	}	

}