import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// Class for the login page
public class LoginPage {
		
  // Page driver
    private final WebDriver driver;

  // Constructor
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    
  // References to the login username and password
        By usernameLocator = By.id("username");
        By passwordLocator = By.id("password");
   
  // Key in Username
    public LoginPage typeUsername(String username) {
        driver.findElement(usernameLocator).sendKeys(username);
        return this;    
    }
    
  // Key in password
    public LoginPage typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);
        return this;    
    }
    
  // Submit the login and go to the next page
    public TestJIRA submitLogin() {
        driver.findElement(passwordLocator).submit();
        driver.get("https://jira.atlassian.com/browse/TST");
        return new TestJIRA(driver);    
    }

  // Login
    public TestJIRA loginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        return submitLogin();
    }
}